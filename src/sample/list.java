package sample;

import java.util.ArrayList;
import java.util.List;

class sample {

	List<Integer> A1 = new ArrayList<Integer>();

	public void saveEvenNumbers(int n) {
		A1 = new ArrayList<Integer>();
		for (int i = 2; i <= n; i++) {
			if (i % 2 == 0) {
				A1.add(i);
			}
		}

		System.out.println(A1);
	}

	public void printEvenNumbers(int n) {
		List<Integer> A2 = new ArrayList<Integer>();
		for (int item : A1) {
			A2.add(item * 2);
		}

	  System.out.println(A2);
	}

	public void printEvenNumber(int n) {
		if (A1.contains(n)) {
			int index = A1.indexOf(n);
			System.out.println(A1.get(index));
		} else {
			System.out.println(0);
		}
	}

}

public class list {

	public static void main(String[] args) {
		sample s = new sample();
		s.saveEvenNumbers(50);
		s.printEvenNumbers(20);
		s.printEvenNumber(40);

	}
}
