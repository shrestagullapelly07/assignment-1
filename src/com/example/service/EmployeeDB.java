package com.example.service;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;



public class EmployeeDB {


	 List<Employee> empDb = new ArrayList<>();   
    
	 
	 public boolean addEmployee(Employee e) {
		 return empDb.add(e);
	 }
	 
	 public boolean deleteEmployee(int empId) {
		 boolean isRemoved=false;
	 
	 
	 Iterator<Employee> it= empDb.iterator();
	 
	   while(it.hasNext()) {
		Employee emp=it.next();
		if(emp.getEmpId()==empId) {
			it.remove();
			isRemoved = true;
			
		}
	 }
	 return isRemoved;
	 }
	 
	 public String ShowPaySlip(int EmpId) {
		String paySlip = "Invalid empId";
		
		for(Employee e: empDb) {
			if(e.getEmpId()==EmpId) {
				paySlip= "payslip for empId" +" "+EmpId + " is" + " "+ e.getEmpSalary();
			}
		}
		return paySlip;
	 }
	 
	 public Employee[] listAll() {
		 Employee[] empArray = new Employee[empDb.size()];
		 for(int i=0; i<empDb.size();i++) {
			 empArray[i]=empDb.get(i);

		 }
		return empArray;

		
	 }
	
}


